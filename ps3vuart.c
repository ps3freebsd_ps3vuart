/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/bus.h>
#include <sys/malloc.h>
#include <sys/lock.h>
#include <sys/mutex.h>
#include <sys/resource.h>
#include <sys/rman.h>

#include <vm/vm.h>
#include <vm/pmap.h>

#include <machine/bus.h>
#include <machine/platform.h>
#include <machine/pmap.h>
#include <machine/resource.h>

#include <powerpc/ps3/ps3-hvcall.h>

#include "ps3vuart.h"

#define PS3VUART_IRQ_BITMAP_SIZE	32

#define PS3VUART_IRQ_RID		0

struct ps3vuart_info {
	void *irq_bitmap;

	uint64_t irq_thread;
	uint64_t irq_outlet;

	struct resource_list resources;
};

struct ps3vuart_softc {
	device_t sc_dev;
};

static MALLOC_DEFINE(M_PS3VUART, "ps3vuart", "PS3 VUART");

static void
ps3vuart_identify(driver_t *driver, device_t parent)
{
	if (strcmp(installed_platform(), "ps3"))
		return;

	if (!device_find_child(parent, "ps3vuart", -1))
		BUS_ADD_CHILD(parent, 0, "ps3vuart", 0);
}

static int 
ps3vuart_probe(device_t dev) 
{
	device_set_desc(dev, "Playstation 3 VUART");

	return (BUS_PROBE_NOWILDCARD);
}

static int
ps3vuart_attach(device_t dev)
{
	struct ps3vuart_softc *sc = device_get_softc(dev);
	struct ps3vuart_info *info;
	device_t child;
	void *bitmap;
	uint64_t thread, outlet, ppe;
	int err;

	sc->sc_dev = dev;

	/* Setup VUART IRQ */

	bitmap = contigmalloc(PS3VUART_IRQ_BITMAP_SIZE, M_PS3VUART,
	    M_NOWAIT | M_ZERO, 0, BUS_SPACE_MAXADDR, 32 /* alignment */,
	    PAGE_SIZE /* boundary */);
	if (!bitmap) {
		device_printf(dev, "Could not allocate VUART IRQ bitmap\n");
		return (ENOMEM);
	}

	lv1_deconfigure_virtual_uart_irq();

	err = lv1_configure_virtual_uart(vtophys(bitmap), &outlet);
	if (err) {
		device_printf(dev, "Could not configure VUART IRQ (%d)\n", err);
		err = ENXIO;
		goto free_bitmap;
	}

	lv1_get_logical_ppe_id(&ppe);

	thread = 32 - fls(mfctrl());

	err = lv1_connect_irq_plug_ext(ppe, thread, outlet, outlet, 0);
	if (err) {
		device_printf(dev, "Could not connect VUART IRQ (%d)\n", err);
		err = ENXIO;
		goto deconfig_vuart_irq;
	}

	/* Setup child */

	info = malloc(sizeof(*info), M_PS3VUART, M_WAITOK | M_ZERO);
	if (!info) {
		device_printf(dev, "Could not allocate bus private variables\n");
		err = ENOMEM;
		goto disconnect_vuart_irq;
	}

	info->irq_bitmap = bitmap;
	info->irq_thread = thread;
	info->irq_outlet = outlet;

	resource_list_init(&info->resources);
	resource_list_add(&info->resources, SYS_RES_IRQ, PS3VUART_IRQ_RID,
	    outlet, outlet, 1);

	child = device_add_child(dev, "ps3vuart_bus", -1);
	if (!child) {
		device_printf(dev, "Could not add child\n");
		err = ENXIO;
		goto free_info;
	}

	device_set_ivars(child, info);

	err = bus_generic_attach(dev);
	if (err)
		goto delete_child;

	return (0);

delete_child:

	device_delete_child(dev, child);

free_info:

	resource_list_free(&info->resources);
	free(info, M_PS3VUART);

disconnect_vuart_irq:

	lv1_disconnect_irq_plug_ext(ppe, thread, outlet);

deconfig_vuart_irq:

	lv1_deconfigure_virtual_uart_irq();

free_bitmap:

	contigfree(bitmap, PS3VUART_IRQ_BITMAP_SIZE, M_PS3VUART);

	return (err);
}

static int
ps3vuart_detach(device_t dev)
{
	device_t child;
	struct ps3vuart_info *info;
	void *bitmap;
	uint64_t thread, outlet, ppe;

	/* Destroy child */

	child = device_find_child(dev, "ps3vuart_bus", -1);

	KASSERT(child, ("could not find child"));

	info = device_get_ivars(child);

	bitmap = info->irq_bitmap;
	thread = info->irq_thread;
	outlet = info->irq_outlet;

	resource_list_free(&info->resources);
	free(info, M_PS3VUART);
	device_delete_child(dev, child);

	/* Destroy VUART IRQ */

	lv1_get_logical_ppe_id(&ppe);
	lv1_disconnect_irq_plug_ext(ppe, thread, outlet);
	lv1_deconfigure_virtual_uart_irq();

	contigfree(bitmap, PS3VUART_IRQ_BITMAP_SIZE, M_PS3VUART);

	return (0);
}

static int
ps3vuart_print_child(device_t dev, device_t child)
{
	struct ps3vuart_info *info = device_get_ivars(child);
	int retval;

	retval = bus_print_child_header(dev, child);
	retval += resource_list_print_type(&info->resources, "irq",
	    SYS_RES_IRQ, "%ld");
	retval += bus_print_child_footer(dev, child);

	return (retval);
}

static int
ps3vuart_read_ivar(device_t dev, device_t child,
    int index, uintptr_t *result)
{
	struct ps3vuart_info *info = device_get_ivars(child);

	switch (index) {
	case PS3VUART_IVAR_IRQ_BITMAP:
		*result = (uintptr_t) info->irq_bitmap;
	break;
	default:
		return (EINVAL);
	}

	return (0);
}

static struct resource *
ps3vuart_alloc_resource(device_t dev, device_t child, int type, int *rid,
    u_long start, u_long end, u_long count, u_int flags)
{
	struct ps3vuart_info *info = device_get_ivars(child);

	flags &= ~RF_ACTIVE;

	switch (type) {
	case SYS_RES_IRQ:
		return (resource_list_alloc(&info->resources, dev, child,
		    type, rid, start, end, count, flags));
	break;
	}

	return (NULL);
}

static int
ps3vuart_release_resource(device_t dev, device_t child, int type, int rid,
    struct resource *r)
{
	struct ps3vuart_info *info = device_get_ivars(child);

	switch (type) {
	case SYS_RES_IRQ:
		return (resource_list_release(&info->resources, dev, child,
		    type, rid, r));
	break;
	}

	return (EINVAL);
}

static int
ps3vuart_activate_resource(device_t dev, device_t child, int type, int rid,
    struct resource *r)
{
	if (type == SYS_RES_IRQ)
		return (bus_activate_resource(dev, type, rid, r));

	return (EINVAL);
}

static device_method_t ps3vuart_methods[] = {
	/* Device interface */
	DEVMETHOD(device_identify,		ps3vuart_identify),
	DEVMETHOD(device_probe,			ps3vuart_probe),
	DEVMETHOD(device_attach,		ps3vuart_attach),
	DEVMETHOD(device_detach,		ps3vuart_detach),

	/* Bus interface */
	DEVMETHOD(bus_print_child,		ps3vuart_print_child),
	DEVMETHOD(bus_read_ivar,		ps3vuart_read_ivar),
	DEVMETHOD(bus_alloc_resource,		ps3vuart_alloc_resource),
	DEVMETHOD(bus_release_resource,		ps3vuart_release_resource),
	DEVMETHOD(bus_activate_resource,	ps3vuart_activate_resource),
	DEVMETHOD(bus_deactivate_resource,	bus_generic_deactivate_resource),
	DEVMETHOD(bus_setup_intr,		bus_generic_setup_intr),
	DEVMETHOD(bus_teardown_intr,		bus_generic_teardown_intr),

	{ 0, 0 }
};

static driver_t ps3vuart_driver = {
	"ps3vuart",
	ps3vuart_methods,
	sizeof(struct ps3vuart_softc)
};

static devclass_t ps3vuart_devclass;

DRIVER_MODULE(ps3vuart, nexus, ps3vuart_driver, ps3vuart_devclass, 0, 0);
MODULE_VERSION(ps3vuart, 1);
