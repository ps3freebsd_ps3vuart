/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/bus.h>
#include <sys/malloc.h>
#include <sys/lock.h>
#include <sys/mutex.h>
#include <sys/resource.h>
#include <sys/rman.h>

#include <vm/vm.h>
#include <vm/pmap.h>

#include <machine/bus.h>
#include <machine/platform.h>
#include <machine/pmap.h>
#include <machine/resource.h>

#include <powerpc/ps3/ps3-hvcall.h>

#include "ps3vuart.h"
#include "ps3vuart_bus.h"

#define PS3VUART_BUS_LOCK_INIT(_sc) \
    mtx_init(&(_sc)->sc_mtx, device_get_nameunit((_sc)->sc_dev), \
        "ps3vuart_bus", MTX_DEF)
#define PS3VUART_BUS_LOCK_DESTROY(_sc)		mtx_destroy(&(_sc)->sc_mtx)
#define PS3VUART_BUS_LOCK(_sc)			mtx_lock(&(_sc)->sc_mtx)
#define	PS3VUART_BUS_UNLOCK(_sc)		mtx_unlock(&(_sc)->sc_mtx)
#define PS3VUART_BUS_ASSERT_LOCKED(_sc) \
    mtx_assert(&(_sc)->sc_mtx, MA_OWNED)
#define PS3VUART_BUS_ASSERT_UNLOCKED(_sc) \
    mtx_assert(&(_sc)->sc_mtx, MA_NOTOWNED)

#define PS3VUART_BUS_PORT_DM			10

#define PS3VUART_BUS_MAX_PORTS			(PS3VUART_BUS_PORT_DM + 1)

#define PS3VUART_BUS_IRQ_RID			0

#define PS3_LAID_GAMEOS				0x1070000002000001ul

struct ps3vuart_bus_info {
	int type;
	int port;
};

struct ps3vuart_bus_intr {
	driver_intr_t *i_ithread;
	void *i_arg;
};

struct ps3vuart_bus_softc {
	device_t sc_dev;

	struct mtx sc_mtx;

	volatile uint64_t *sc_irq_bitmap;
	int sc_irq_id;
	struct resource	*sc_irq;
	void *sc_irq_ctx;

	struct ps3vuart_bus_intr sc_intr[PS3VUART_BUS_MAX_PORTS];
};

static void ps3vuart_bus_intr(void *arg);

static int ps3vuart_bus_add_children(struct ps3vuart_bus_softc *sc);
static void ps3vuart_bus_delete_children(struct ps3vuart_bus_softc *sc);
static int ps3vuart_bus_add_child(struct ps3vuart_bus_softc *sc,
    int type, int port);

static MALLOC_DEFINE(M_PS3VUART_BUS, "ps3vuart_bus", "PS3 VUART Bus");

static int 
ps3vuart_bus_probe(device_t dev) 
{
	device_set_desc(dev, "Playstation 3 VUART Bus");

	return (BUS_PROBE_SPECIFIC);
}

static int
ps3vuart_bus_attach(device_t dev)
{
	struct ps3vuart_bus_softc *sc = device_get_softc(dev);
	int err;

	sc->sc_dev = dev;

	PS3VUART_BUS_LOCK_INIT(sc);

	/* Setup IRQ */

	sc->sc_irq_bitmap = ps3vuart_get_irq_bitmap(dev);

	sc->sc_irq_id = PS3VUART_BUS_IRQ_RID;
	sc->sc_irq = bus_alloc_resource_any(dev, SYS_RES_IRQ,
	    &sc->sc_irq_id, RF_ACTIVE);
	if (!sc->sc_irq) {
		device_printf(dev, "Could not allocate IRQ\n");
		err = ENXIO;
		goto destroy_lock;
	}

	err = bus_setup_intr(dev, sc->sc_irq,
	    INTR_TYPE_MISC | INTR_MPSAFE,
	    NULL, ps3vuart_bus_intr, sc, &sc->sc_irq_ctx);
	if (err) {
		device_printf(dev, "Could not setup IRQ (%d)\n", err);
		goto release_irq;
	}

	/* Setup children */

	err = ps3vuart_bus_add_children(sc);
	if (err) {
		device_printf(dev, "Could not add children (%d)\n", err);
		goto teardown_irq;
	}

	err = bus_generic_attach(sc->sc_dev);
	if (err)
		goto delete_children;

	return (0);

delete_children:

	ps3vuart_bus_delete_children(sc);

teardown_irq:

	bus_teardown_intr(dev, sc->sc_irq, sc->sc_irq_ctx);

release_irq:

	bus_release_resource(dev, SYS_RES_IRQ, sc->sc_irq_id, sc->sc_irq);

destroy_lock:

	PS3VUART_BUS_LOCK_DESTROY(sc);

	return (err);
}

static int
ps3vuart_bus_detach(device_t dev)
{
	struct ps3vuart_bus_softc *sc = device_get_softc(dev);

	/* Destroy children */

	ps3vuart_bus_delete_children(sc);

	/* Free IRQ */

	bus_teardown_intr(dev, sc->sc_irq, sc->sc_irq_ctx);
	bus_release_resource(dev, SYS_RES_IRQ, sc->sc_irq_id, sc->sc_irq);

	sc->sc_irq_bitmap = NULL;

	PS3VUART_BUS_LOCK_DESTROY(sc);

	return (0);
}

static int
ps3vuart_bus_print_child(device_t dev, device_t child)
{
	struct ps3vuart_bus_info *info = device_get_ivars(child);
	int retval;

	retval = bus_print_child_header(dev, child);
	retval += printf(" at port %d", info->port);
	retval += bus_print_child_footer(dev, child);

	return (retval);
}

static int
ps3vuart_bus_read_ivar(device_t dev, device_t child,
    int index, uintptr_t *result)
{
	struct ps3vuart_bus_info *info = device_get_ivars(child);

	switch (index) {
	case PS3VUART_BUS_IVAR_TYPE:
		*result = info->type;
	break;
	case PS3VUART_BUS_IVAR_PORT:
		*result = info->port;
	break;
	default:
		return (EINVAL);
	}

	return (0);
}

static struct resource *
ps3vuart_bus_alloc_resource(device_t dev, device_t child, int type, int *rid,
    u_long start, u_long end, u_long count, u_int flags)
{
	struct ps3vuart_bus_softc *sc = device_get_softc(dev);
	struct resource *r = NULL;

	switch (type) {
	case SYS_RES_IRQ:
		if (*rid == PS3VUART_BUS_IRQ_RID)
			r = sc->sc_irq;
	break;
	}

	return (r);
}

static int
ps3vuart_bus_release_resource(device_t dev, device_t child, int type, int rid,
    struct resource *r)
{
	switch (type) {
	case SYS_RES_IRQ:
		if (rid != PS3VUART_BUS_IRQ_RID)
			return (ENOENT);
		else
			return (0);
	break;
	}

	return (EINVAL);
}

static int
ps3vuart_bus_setup_intr(device_t dev, device_t child, struct resource *irq, 
    int flags, driver_filter_t *filter, driver_intr_t *ithread, 
    void *arg, void **cookiep)
{
	struct ps3vuart_bus_softc *sc = device_get_softc(dev);
	int port = ps3vuart_bus_get_port(child);

	if (filter)
		return (EINVAL);

	sc->sc_intr[port].i_ithread = ithread;
	sc->sc_intr[port].i_arg = arg;

	return (0);
}

static int
ps3vuart_bus_teardown_intr(device_t dev, device_t child, struct resource *irq,
    void *cookie)
{
	struct ps3vuart_bus_softc *sc = device_get_softc(dev);
	int port = ps3vuart_bus_get_port(child);

	sc->sc_intr[port].i_ithread = NULL;
	sc->sc_intr[port].i_arg = NULL;

	return (0);
}

static int
ps3vuart_bus_child_location_str(device_t dev, device_t child,
    char *buf, size_t buflen)
{
	int port = ps3vuart_bus_get_port(child);

	snprintf(buf, buflen, "port=%d", port);

	return (0);
}

static void
ps3vuart_bus_intr(void *arg)
{
	struct ps3vuart_bus_softc *sc = (struct ps3vuart_bus_softc *) arg;
	int port;

	PS3VUART_BUS_LOCK(sc);

	while ((port = 64 - ffsl(sc->sc_irq_bitmap[0])) != 64) {
		KASSERT(port < PS3VUART_BUS_MAX_PORTS,
		    ("invalid port %d", port));
		KASSERT(sc->sc_intr[port].i_ithread != NULL,
		    ("unhandled IRQ on port %d", port));

		/* Dispatch IRQ to child */

		sc->sc_intr[port].i_ithread(sc->sc_intr[port].i_arg);
	}

	PS3VUART_BUS_UNLOCK(sc);
}

static int
ps3vuart_bus_add_children(struct ps3vuart_bus_softc *sc)
{
	uint64_t lpar_id, port, laid, junk;
	int err;

	lv1_get_logical_partition_id(&lpar_id);

	/* Attach AV */

	err = lv1_get_repository_node_value(lpar_id,
	    (lv1_repository_string("bi") >> 32),
	    lv1_repository_string("vir_uart"),
	    lv1_repository_string("port"),
	    lv1_repository_string("avset"), &port, &junk);
	if (!err) {
		err = ps3vuart_bus_add_child(sc, PS3VUART_BUS_TYPE_AV, port);
		if (err)
			goto fail;
	}

	/* Attach System Manager */

	err = lv1_get_repository_node_value(lpar_id,
	    (lv1_repository_string("bi") >> 32),
	    lv1_repository_string("vir_uart"),
	    lv1_repository_string("port"),
	    lv1_repository_string("sysmgr"), &port, &junk);
	if (!err) {
		err = ps3vuart_bus_add_child(sc, PS3VUART_BUS_TYPE_SM, port);
		if (err)
			goto fail;
	}

	/* Attach Dispatcher Manager which is only present in GameOS LPAR */

	err = lv1_get_repository_node_value(PS3_LPAR_ID_PME,
	    (lv1_repository_string("ss") >> 32),
	    lv1_repository_string("laid"), 2, 0, &laid, &junk);
	if (!err && (laid == PS3_LAID_GAMEOS)) {
		/* Dispatcher Manager port is not available in node repository */
		err = ps3vuart_bus_add_child(sc, PS3VUART_BUS_TYPE_DM,
		    PS3VUART_BUS_PORT_DM);
		if (err)
			goto fail;
	}

	return (0);

fail:

	ps3vuart_bus_delete_children(sc);

	return (err);
}

static void
ps3vuart_bus_delete_children(struct ps3vuart_bus_softc *sc)
{
	device_t *children;
	int nchildren, i;

	if (device_get_children(sc->sc_dev, &children, &nchildren))
		return;

	for (i = 0; i < nchildren; i++) {
		free(device_get_ivars(children[i]), M_PS3VUART_BUS);
		device_delete_child(sc->sc_dev, children[i]);
	}

	free(children, M_TEMP);
}

static int
ps3vuart_bus_add_child(struct ps3vuart_bus_softc *sc,
    int type, int port)
{
	struct ps3vuart_bus_info *info;
	device_t child;
	int err;

	info = malloc(sizeof(*info), M_PS3VUART_BUS, M_WAITOK | M_ZERO);
	if (!info)
		return (ENOMEM);

	info->type = type;
	info->port = port;

	child = device_add_child(sc->sc_dev, NULL, -1);
	if (!child) {
		err = ENOMEM;
		goto fail;
	}

	device_set_ivars(child, info);

	return (0);

fail:

	free(info, M_PS3VUART_BUS);

	return (err);
}

static device_method_t ps3vuart_bus_methods[] = {
	/* Device interface */
	DEVMETHOD(device_probe,			ps3vuart_bus_probe),
	DEVMETHOD(device_attach,		ps3vuart_bus_attach),
	DEVMETHOD(device_detach,		ps3vuart_bus_detach),

	/* Bus interface */
	DEVMETHOD(bus_print_child,		ps3vuart_bus_print_child),
	DEVMETHOD(bus_read_ivar,		ps3vuart_bus_read_ivar),
	DEVMETHOD(bus_alloc_resource,		ps3vuart_bus_alloc_resource),
	DEVMETHOD(bus_release_resource,		ps3vuart_bus_release_resource),
	DEVMETHOD(bus_setup_intr,		ps3vuart_bus_setup_intr),
	DEVMETHOD(bus_teardown_intr,		ps3vuart_bus_teardown_intr),
	DEVMETHOD(bus_child_location_str,	ps3vuart_bus_child_location_str),

	{ 0, 0 }
};

static driver_t ps3vuart_bus_driver = {
	"ps3vuart_bus",
	ps3vuart_bus_methods,
	sizeof(struct ps3vuart_bus_softc)
};

static devclass_t ps3vuart_bus_devclass;

DRIVER_MODULE(ps3vuart_bus, ps3vuart, ps3vuart_bus_driver, ps3vuart_bus_devclass, 0, 0);
MODULE_VERSION(ps3vuart_bus, 1);
