/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/conf.h>
#include <sys/bus.h>
#include <sys/kthread.h>
#include <sys/malloc.h>
#include <sys/lock.h>
#include <sys/mutex.h>
#include <sys/uio.h>
#include <sys/resource.h>
#include <sys/rman.h>
#include <sys/reboot.h>

#include <vm/vm.h>
#include <vm/pmap.h>

#include <machine/bus.h>
#include <machine/platform.h>
#include <machine/pmap.h>
#include <machine/resource.h>

#include "ps3vuart_bus.h"
#include "ps3vuart_port.h"
#include "ps3sm_msg.h"
#include "ps3sm.h"

#define PS3SM_LOCK_INIT(_sc) \
    mtx_init(&(_sc)->sc_mtx, device_get_nameunit((_sc)->sc_dev), \
        "ps3sm", MTX_DEF)
#define PS3SM_LOCK_DESTROY(_sc)		mtx_destroy(&(_sc)->sc_mtx)
#define PS3SM_LOCK(_sc)			mtx_lock(&(_sc)->sc_mtx)
#define	PS3SM_UNLOCK(_sc)		mtx_unlock(&(_sc)->sc_mtx)
#define PS3SM_ASSERT_LOCKED(_sc)	mtx_assert(&(_sc)->sc_mtx, MA_OWNED)
#define PS3SM_ASSERT_UNLOCKED(_sc)	mtx_assert(&(_sc)->sc_mtx, MA_NOTOWNED)

struct ps3sm_packet {
	TAILQ_ENTRY(ps3sm_packet) sp_queue;

	struct ps3sm_header sp_hdr;
	char sp_data[0];
};

TAILQ_HEAD(ps3sm_packetq, ps3sm_packet);

struct ps3sm_softc {
	device_t sc_dev;

	struct mtx sc_mtx;

	int sc_irq_id;
	struct resource	*sc_irq;
	void *sc_irq_ctx;

	struct ps3vuart_port sc_port;

	struct cdev *sc_cdev;

	struct ps3sm_header sc_pkthdr;
	struct ps3sm_packetq sc_pktq;

	int sc_async_read;
	struct proc *sc_async_task;

	int sc_running;
};

static void ps3sm_intr(void *arg);
static void ps3sm_async_task(void *arg);
static int ps3sm_read_packet(struct ps3sm_softc *sc,
    struct ps3sm_packet **pkt);
static int ps3sm_init(struct ps3sm_softc *sc);
static int ps3sm_fini(struct ps3sm_softc *sc);

static int ps3sm_dev_open(struct cdev *dev, int flags, int mode,
    struct thread *td);
static int ps3sm_dev_close(struct cdev *dev, int flags, int mode,
    struct thread *td);
static int ps3sm_dev_read(struct cdev *dev, struct uio *uio, int ioflag);
static int ps3sm_dev_write(struct cdev *dev, struct uio *uio, int ioflag);
static int ps3sm_dev_poll(struct cdev *dev, int events, struct thread *td);

static MALLOC_DEFINE(M_PS3SM, "ps3sm", "PS3 SM");

static struct cdevsw ps3sm_cdevsw = {
	.d_version	= D_VERSION,
	.d_open		= ps3sm_dev_open,
	.d_close	= ps3sm_dev_close,
	.d_read		= ps3sm_dev_read,
	.d_write	= ps3sm_dev_write,
	.d_poll		= ps3sm_dev_poll,
	.d_name		= "ps3sm",
};

static device_t ps3sm_dev = NULL;

static int 
ps3sm_probe(device_t dev) 
{
	if (ps3vuart_bus_get_type(dev) != PS3VUART_BUS_TYPE_SM)
		return (ENXIO);

	device_set_desc(dev, "Playstation 3 SM");

	return (BUS_PROBE_SPECIFIC);
}

static int
ps3sm_attach(device_t dev)
{
	struct ps3sm_softc *sc = device_get_softc(dev);
	int err;

	sc->sc_dev = dev;

	PS3SM_LOCK_INIT(sc);

	PS3SM_LOCK(sc);

	/* Setup IRQ */

	sc->sc_irq_id = 0;
	sc->sc_irq = bus_alloc_resource_any(dev, SYS_RES_IRQ,
	    &sc->sc_irq_id, RF_ACTIVE);
	if (!sc->sc_irq) {
		device_printf(dev, "Could not allocate IRQ\n");
		err = ENXIO;
		goto destroy_lock;
	}

	err = bus_setup_intr(dev, sc->sc_irq,
	    INTR_TYPE_MISC | INTR_MPSAFE,
	    NULL, ps3sm_intr, sc, &sc->sc_irq_ctx);
	if (err) {
		device_printf(dev, "Could not setup IRQ (%d)\n", err);
		goto release_irq;
	}

	/* Setup VUART port */

	err = ps3vuart_port_init(&sc->sc_port, ps3vuart_bus_get_port(dev));
	if (err) {
		device_printf(dev, "Could not setup VUART port (%d)\n", err);
		goto teardown_irq;
	}

	/* Setup SM */

	err = ps3sm_init(sc);
	if (err) {
		device_printf(dev, "Could not setup SM (%d)\n", err);
		goto fini_vuart_port;
	}

	/* Create char device */

	sc->sc_cdev = make_dev(&ps3sm_cdevsw, 0, UID_ROOT, GID_WHEEL, 0600,
	    "%s", "ps3sm");
	if (!sc->sc_cdev) {
		device_printf(dev, "Could not create char device\n");
		err = ENOMEM;
		goto fini_sm;
	}

	sc->sc_cdev->si_drv1 = sc;

	TAILQ_INIT(&sc->sc_pktq);

	sc->sc_async_read = 1;

	/* Create async task */

	kproc_create(&ps3sm_async_task, sc, &sc->sc_async_task, 0, 0,
	    "PS3 SM async task");

	sc->sc_running = 1;

	ps3sm_dev = dev;

	PS3SM_UNLOCK(sc);

	return (0);

fini_sm:

	ps3sm_fini(sc);

fini_vuart_port:

	ps3vuart_port_fini(&sc->sc_port);

teardown_irq:

	bus_teardown_intr(dev, sc->sc_irq, sc->sc_irq_ctx);

release_irq:

	bus_release_resource(dev, SYS_RES_IRQ, sc->sc_irq_id, sc->sc_irq);

destroy_lock:

	PS3SM_UNLOCK(sc);

	PS3SM_LOCK_DESTROY(sc);

	return (err);
}

static int
ps3sm_detach(device_t dev)
{
	struct ps3sm_softc *sc = device_get_softc(dev);
	struct ps3sm_packet *pkt;

	PS3SM_LOCK(sc);

	ps3sm_dev = NULL;

	/* Stop async task */

	sc->sc_running = 0;
	wakeup(sc);
	while (sc->sc_running != -1)
		msleep(sc, &sc->sc_mtx, 0, "detach", 0);

	/* Free packet queue */

	while ((pkt = TAILQ_FIRST(&sc->sc_pktq))) {
		TAILQ_REMOVE(&sc->sc_pktq, pkt, sp_queue);
		free(pkt, M_PS3SM);
	}

	/* Destroy char device */

	destroy_dev(sc->sc_cdev);

	/* Shutdown SM */

	ps3sm_fini(sc);

	/* Free IRQ */

	bus_teardown_intr(dev, sc->sc_irq, sc->sc_irq_ctx);
	bus_release_resource(dev, SYS_RES_IRQ, sc->sc_irq_id, sc->sc_irq);

	/* Destroy VUART port */

	ps3vuart_port_fini(&sc->sc_port);

	PS3SM_UNLOCK(sc);

	PS3SM_LOCK_DESTROY(sc);

	return (0);
}

static void
ps3sm_intr(void *arg)
{
	struct ps3sm_softc *sc = arg;
	struct ps3sm_packet *pkt = NULL;
	int err;

	PS3SM_LOCK(sc);

	ps3vuart_port_intr(&sc->sc_port);

	if (sc->sc_async_read) {
		err = ps3sm_read_packet(sc, &pkt);
		if (!err) {
			TAILQ_INSERT_TAIL(&sc->sc_pktq, pkt, sp_queue);
			wakeup(sc);
		}
	}

	PS3SM_UNLOCK(sc);
}

static void
ps3sm_async_task(void *arg)
{
	struct ps3sm_softc *sc = (struct ps3sm_softc *) arg;
	struct ps3sm_packet *pkt;

	while (sc->sc_running) {
		PS3SM_LOCK(sc);

		do {
			pkt = TAILQ_FIRST(&sc->sc_pktq);
			if (!pkt)
				msleep(sc, &sc->sc_mtx, 0, "packet queue", 0);
		} while (!pkt && sc->sc_running);

		if (pkt)
			TAILQ_REMOVE(&sc->sc_pktq, pkt, sp_queue);

		PS3SM_UNLOCK(sc);

		if (!sc->sc_running) {
			if (pkt)
				free(pkt, M_PS3SM);
			break;
		}

		switch (pkt->sp_hdr.sid) {
		case PS3SM_SID_EXT_EVENT:
		{
			struct ps3sm_ext_event *ext_event =
			    (struct ps3sm_ext_event *) &pkt->sp_hdr;

			switch (ext_event->type) {
			case PS3SM_EXT_EVENT_POWER_PRESSED:
				device_printf(sc->sc_dev,
				    "power button pressed\n");

				shutdown_nice(0);
			break;
			case PS3SM_EXT_EVENT_POWER_RELEASED:
				device_printf(sc->sc_dev,
				    "power button released\n");
			break;
			case PS3SM_EXT_EVENT_THERMAL_ALERT:
				device_printf(sc->sc_dev,
				    "thermal alert\n");
			break;
			case PS3SM_EXT_EVENT_THERMAL_CLEARED:
				device_printf(sc->sc_dev,
				    "thermal cleared\n");
			break;
			}
		}
		break;
		case PS3SM_SID_REQ_ERROR:
		{
			struct ps3sm_req_error *req_error =
			    (struct ps3sm_req_error *) &pkt->sp_hdr;

			device_printf(sc->sc_dev,
			    "request error: payload length %d "
			    "sid 0x%04x tag 0x%08x\n",
			    PS3SM_HDR(req_error)->payload_length,
			    PS3SM_HDR(req_error)->sid,
			    PS3SM_HDR(req_error)->tag);
		}
		break;
		default:
			device_printf(sc->sc_dev,
			    "async packet: payload length %d "
			    "sid 0x%04x tag 0x%08x\n",
			    pkt->sp_hdr.payload_length,
			    pkt->sp_hdr.sid,
			    pkt->sp_hdr.tag);
		break;
		}

		free(pkt, M_PS3SM);
	}

	PS3SM_LOCK(sc);
	sc->sc_running = -1;
	wakeup(sc);
	PS3SM_UNLOCK(sc);

	kproc_exit(1);
}

static int
ps3sm_read_packet(struct ps3sm_softc *sc, struct ps3sm_packet **pkt)
{
	uint64_t bytes;
	int read;
	int err;

	PS3SM_ASSERT_LOCKED(sc);

	/* Read packet header */

	if (!sc->sc_pkthdr.length) {
		err = ps3vuart_port_get_param(&sc->sc_port,
		    PS3VUART_PORT_PARAM_RX_BYTES, &bytes);
		if (err)
			return (err);

		if (bytes < sizeof(struct ps3sm_header))
			return (EAGAIN);

		err = ps3vuart_port_read(&sc->sc_port,
		    (char *) &sc->sc_pkthdr, sizeof(sc->sc_pkthdr), &read);
		if (err)
			return (err);
	}

	/* Read packet body */

	if (sc->sc_pkthdr.length) {
		err = ps3vuart_port_get_param(&sc->sc_port,
		    PS3VUART_PORT_PARAM_RX_BYTES, &bytes);
		if (err)
			return (err);

		if (bytes < sc->sc_pkthdr.length)
			return (EAGAIN);

		*pkt = malloc(sizeof(struct ps3sm_packet) +
		    sc->sc_pkthdr.length, M_PS3SM, M_WAITOK);
		if (!(*pkt))
			return (ENOMEM);

		err = ps3vuart_port_read(&sc->sc_port,
		    (char *) (*pkt)->sp_data, sc->sc_pkthdr.length, &read);
		if (err) {
			free(*pkt, M_PS3SM);
			*pkt = NULL;
			return (err);
		}

		bcopy(&sc->sc_pkthdr, &(*pkt)->sp_hdr, sizeof(sc->sc_pkthdr));

		sc->sc_pkthdr.length = 0;

		return (0);
	}

	return (EAGAIN);
}

static int
ps3sm_init(struct ps3sm_softc *sc)
{
	struct ps3sm_set_attr set_attr;
	int written;
	int err;

	PS3SM_ASSERT_LOCKED(sc);

	/* Set attribute */

	ps3sm_init_header(PS3SM_HDR(&set_attr),
	    sizeof(set_attr) - sizeof(struct ps3sm_header),
	    PS3SM_SID_SET_ATTR, 0);
	set_attr.version = PS3SM_SET_ATTR_VERSION;
	set_attr.attrs = PS3SM_ATTR_POWER | PS3SM_ATTR_THERMAL;

	err = ps3vuart_port_write(&sc->sc_port, (char *) &set_attr,
	    sizeof(set_attr), &written);
	if (err)
		return (err);

	return (0);
}

static int
ps3sm_fini(struct ps3sm_softc *sc)
{
	struct ps3sm_set_attr set_attr;
	int written;
	int err;

	PS3SM_ASSERT_LOCKED(sc);

	/* Set attribute */

	ps3sm_init_header(PS3SM_HDR(&set_attr),
	    sizeof(set_attr) - sizeof(struct ps3sm_header),
	    PS3SM_SID_SET_ATTR, 0);
	set_attr.version = PS3SM_SET_ATTR_VERSION;
	set_attr.attrs = 0;

	err = ps3vuart_port_write(&sc->sc_port, (char *) &set_attr,
	    sizeof(set_attr), &written);
	if (err)
		return (err);

	return (0);
}

int
ps3sm_ctl_led(uint8_t arg1, uint8_t arg2, uint8_t arg3, uint8_t arg4)
{
	struct ps3sm_softc *sc;
	struct ps3sm_ctl_led ctl_led;
	int written;
	int err;

	if (!ps3sm_dev)
		return (ENODEV);

	sc = device_get_softc(ps3sm_dev);

	PS3SM_LOCK(sc);

	ps3sm_init_header(PS3SM_HDR(&ctl_led),
	    sizeof(ctl_led) - sizeof(struct ps3sm_header),
	    PS3SM_SID_CTL_LED, 0);
	ctl_led.version = PS3SM_CTL_LED_VERSION;
	ctl_led.arg1 = arg1;
	ctl_led.arg2 = arg2;
	ctl_led.arg3 = arg3;
	ctl_led.arg4 = arg4;

	err = ps3vuart_port_write(&sc->sc_port, (char *) &ctl_led,
	    sizeof(ctl_led), &written);

	PS3SM_UNLOCK(sc);

	return (err);
}

int
ps3sm_ring_buzzer(uint8_t arg1, uint8_t arg2, uint32_t arg3)
{
	struct ps3sm_softc *sc;
	struct ps3sm_ring_buzzer ring_buzzer;
	int written;
	int err;

	if (!ps3sm_dev)
		return (ENODEV);

	sc = device_get_softc(ps3sm_dev);

	PS3SM_LOCK(sc);

	ps3sm_init_header(PS3SM_HDR(&ring_buzzer),
	    sizeof(ring_buzzer) - sizeof(struct ps3sm_header),
	    PS3SM_SID_RING_BUZZER, 0);
	ring_buzzer.version = PS3SM_RING_BUZZER_VERSION;
	ring_buzzer.arg1 = arg1;
	ring_buzzer.arg2 = arg2;
	ring_buzzer.arg3 = arg3;

	err = ps3vuart_port_write(&sc->sc_port, (char *) &ring_buzzer,
	    sizeof(ring_buzzer), &written);

	PS3SM_UNLOCK(sc);

	return (err);
}

static int
ps3sm_dev_open(struct cdev *dev, int flags, int mode,
    struct thread *td)
{
	struct ps3sm_softc *sc = dev->si_drv1;

	PS3SM_LOCK(sc);

	/* Disable async packet reading in kernel */

	sc->sc_async_read = 0;

	PS3SM_UNLOCK(sc);

	return (0);
}

static int
ps3sm_dev_close(struct cdev *dev, int flags, int mode,
    struct thread *td)
{
	struct ps3sm_softc *sc = dev->si_drv1;

	PS3SM_LOCK(sc);

	/* Enable async packet reading in kernel */

	sc->sc_async_read = 1;

	PS3SM_UNLOCK(sc);

	return (0);
}

static int
ps3sm_dev_read(struct cdev *dev, struct uio *uio, int ioflag)
{
#define BUFSZ	4096

	struct ps3sm_softc *sc = dev->si_drv1;
	unsigned char *buf;
	int read;
	int err = 0;

	PS3SM_LOCK(sc);

	if (!uio->uio_resid)
		goto out;

	buf = malloc(BUFSZ, M_PS3SM, M_WAITOK);
	if (!buf) {
		err = ENOMEM;
		goto out;
	}

	while (uio->uio_resid) {
		err = ps3vuart_port_read(&sc->sc_port, buf,
		    min(uio->uio_resid, BUFSZ), &read);
		if (err || !read)
			break;

		err = uiomove(buf, read, uio);
		if (!err)
			break;
	}

	free(buf, M_PS3SM);

out:

	PS3SM_UNLOCK(sc);

	return (err);

#undef BUFSZ
}

static int
ps3sm_dev_write(struct cdev *dev, struct uio *uio, int ioflag)
{
#define BUFSZ	4096

	struct ps3sm_softc *sc = dev->si_drv1;
	unsigned char *buf;
	int bytes, written;
	int err = 0;

	PS3SM_LOCK(sc);

	if (!uio->uio_resid)
		goto out;

	buf = malloc(BUFSZ, M_PS3SM, M_WAITOK);
	if (!buf) {
		err = ENOMEM;
		goto out;
	}

	while (uio->uio_resid) {
		bytes = min(uio->uio_resid, BUFSZ);

		err = uiomove(buf, bytes, uio);
		if (err)
			break;

		err = ps3vuart_port_write(&sc->sc_port, buf, bytes, &written);
		if (err || !written)
			break;
	}

	free(buf, M_PS3SM);

out:

	PS3SM_UNLOCK(sc);

	return (err);

#undef BUFSZ
}

static int
ps3sm_dev_poll(struct cdev *dev, int events, struct thread *td)
{
	struct ps3sm_softc *sc = dev->si_drv1;

	PS3SM_LOCK(sc);

	/* XXX: implement */

	PS3SM_UNLOCK(sc);

	return (0);
}

static device_method_t ps3sm_methods[] = {
	/* Device interface */
	DEVMETHOD(device_probe,		ps3sm_probe),
	DEVMETHOD(device_attach,	ps3sm_attach),
	DEVMETHOD(device_detach,	ps3sm_detach),

	{ 0, 0 }
};

static driver_t ps3sm_driver = {
	"ps3sm",
	ps3sm_methods,
	sizeof(struct ps3sm_softc)
};

static devclass_t ps3sm_devclass;

DRIVER_MODULE(ps3sm, ps3vuart_bus, ps3sm_driver, ps3sm_devclass, 0, 0);
MODULE_DEPEND(ps3sm, ps3vuart_port, 1, 1, 1);
MODULE_VERSION(ps3sm, 1);
