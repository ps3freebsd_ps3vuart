/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3VUART_PORT_H
#define _PS3VUART_PORT_H

enum ps3vuart_port_param {
	PS3VUART_PORT_PARAM_TX_TRIG	= 0, /* rw */
	PS3VUART_PORT_PARAM_RX_TRIG	= 1, /* rw */
	PS3VUART_PORT_PARAM_INTR_MASK	= 2, /* rw */
	PS3VUART_PORT_PARAM_RX_BUFSIZE	= 3, /* ro */
	PS3VUART_PORT_PARAM_RX_BYTES	= 4, /* ro */
	PS3VUART_PORT_PARAM_TX_BUFSIZE	= 5, /* ro */
	PS3VUART_PORT_PARAM_TX_BYTES	= 6, /* ro */
	PS3VUART_PORT_PARAM_INTR_STAT	= 7, /* ro */
};

enum ps3vuart_port_intr {
	PS3VUART_PORT_INTR_TX		= (1ul << 0),
	PS3VUART_PORT_INTR_RX		= (1ul << 1),
	PS3VUART_PORT_INTR_DISCONNECT	= (1ul << 2),
	PS3VUART_PORT_INTR_CONNECT	= (1ul << 3),
};

struct ps3vuart_port_buf {
	TAILQ_ENTRY(ps3vuart_port_buf) vb_queue;

	char *vb_head;
	char *vb_tail;
	char vb_data[0];
};

TAILQ_HEAD(ps3vuart_port_bufq, ps3vuart_port_buf);

struct ps3vuart_port_stat {
	unsigned long vs_rx_bytes;
	unsigned long vs_tx_bytes;
	unsigned long vs_rx_intrs;
	unsigned long vs_tx_intrs;
};

struct ps3vuart_port {
	int vp_port;

	struct mtx vp_mtx;

	uint64_t vp_intr_mask;

	struct ps3vuart_port_bufq vp_rx_bufq;
	int vp_rx_bufq_bytes;

	struct ps3vuart_port_bufq vp_tx_bufq;
	int vp_tx_bufq_bytes;

	struct ps3vuart_port_stat vp_stat;
};

int ps3vuart_port_init(struct ps3vuart_port *vp, int port);

void ps3vuart_port_fini(struct ps3vuart_port *vp);

int ps3vuart_port_write(struct ps3vuart_port *vp, const char *buf, int size,
    int *written);

int ps3vuart_port_read(struct ps3vuart_port *vp, char *buf, int size,
    int *read);

int ps3vuart_port_set_param(struct ps3vuart_port *vp, int param, uint64_t val);

int ps3vuart_port_get_param(struct ps3vuart_port *vp, int param, uint64_t *val);

void ps3vuart_port_intr(struct ps3vuart_port *vp);

void ps3vuart_port_get_stat(struct ps3vuart_port *vp,
    struct ps3vuart_port_stat *stat);

#endif /* _PS3VUART_PORT_H */
