/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/malloc.h>
#include <sys/lock.h>
#include <sys/mutex.h>

#include <vm/vm.h>
#include <vm/pmap.h>

#include <powerpc/ps3/ps3-hvcall.h>

#include "ps3vuart_port.h"

#define PS3VUART_PORT_LOCK_INIT(_vp) \
    mtx_init(&(_vp)->vp_mtx, "ps3vuart_port", NULL, MTX_DEF)
#define PS3VUART_PORT_LOCK_DESTROY(_vp)		mtx_destroy(&(_vp)->vp_mtx)
#define PS3VUART_PORT_LOCK(_vp)			mtx_lock(&(_vp)->vp_mtx)
#define	PS3VUART_PORT_UNLOCK(_vp)		mtx_unlock(&(_vp)->vp_mtx)
#define PS3VUART_PORT_ASSERT_LOCKED(_vp)	mtx_assert(&(_vp)->vp_mtx, MA_OWNED)
#define PS3VUART_PORT_ASSERT_UNLOCKED(_vp)	mtx_assert(&(_vp)->vp_mtx, MA_NOTOWNED)

static MALLOC_DEFINE(M_PS3VUART_PORT, "ps3vuart_port", "PS3 VUART Port");

static int
ps3vuart_port_set_intr_mask(struct ps3vuart_port *vp, uint64_t mask)
{
	int err;

	PS3VUART_PORT_ASSERT_LOCKED(vp);

	if (vp->vp_intr_mask == mask)
		return (0);

	err = lv1_set_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_INTR_MASK, mask);
	if (err)
		return (err);

	vp->vp_intr_mask = mask;

	return (0);
}

static int
p3vuart_port_discard_rx_bytes(struct ps3vuart_port *vp, int bytes,
    int *discarded)
{
	uint64_t waiting, read;
	void *buf;
	int err;

	PS3VUART_PORT_ASSERT_LOCKED(vp);

	if (!bytes) {
		err = lv1_get_virtual_uart_param(vp->vp_port,
		    PS3VUART_PORT_PARAM_RX_BYTES, &waiting);
		if (err)
			return (err);

		bytes = waiting;
	}

	if (!bytes) {
		if (discarded)
			*discarded = 0;
		return (0);
	}

	bytes += 128;

	buf = malloc(bytes, M_PS3VUART_PORT, M_WAITOK);
	if (!buf)
		return (ENOMEM);

	err = lv1_read_virtual_uart(vp->vp_port, vtophys(buf),
	    bytes, &read);

	free(buf, M_PS3VUART_PORT);

	if (err)
		return (err);

	if (discarded)
		*discarded = read;

	return (0);
}

static int
ps3vuart_port_push(struct ps3vuart_port *vp, int *pushed)
{
	struct ps3vuart_port_buf *vb, *tmp;
	uint64_t written;
	int bytes, err;

	PS3VUART_PORT_ASSERT_LOCKED(vp);

	if (pushed)
		*pushed = 0;

	TAILQ_FOREACH_SAFE(vb, &vp->vp_tx_bufq, vb_queue, tmp) {
		bytes = vb->vb_tail - vb->vb_head;

		err = lv1_write_virtual_uart(vp->vp_port,
		    vtophys(vb->vb_head), bytes, &written);
		if (err)
			return (err);

		vp->vp_tx_bufq_bytes -= written;
		vp->vp_stat.vs_tx_bytes += written;

		if (pushed)
			*pushed += written;

		if (written < bytes) {
			/* VUART Tx buffer is full */
			vb->vb_head += written;
			return (0);
		}

		TAILQ_REMOVE(&vp->vp_tx_bufq, vb, vb_queue);
		free(vb, M_PS3VUART_PORT);
	}

	/* Tx queue is empty, disable Tx interrupts */

	err = ps3vuart_port_set_intr_mask(vp,
	    vp->vp_intr_mask & ~PS3VUART_PORT_INTR_TX);

	return (err);
}

static int
ps3vuart_port_pull(struct ps3vuart_port *vp, int *pulled)
{
	struct ps3vuart_port_buf *vb;
	uint64_t waiting, read;
	int err;

	PS3VUART_PORT_ASSERT_LOCKED(vp);

	err = lv1_get_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_RX_BYTES, &waiting);
	if (err)
		return (err);

	if (!waiting) {
		if (pulled)
			*pulled = 0;
		return (0);
	}

	waiting += 128;

	vb = malloc(sizeof(*vb) + waiting, M_PS3VUART_PORT, M_WAITOK);
	if (!vb)
		return (ENOMEM);

	err = lv1_read_virtual_uart(vp->vp_port,
	    vtophys(vb->vb_data), waiting, &read);
	if (err)
		return (err);

	vb->vb_head = vb->vb_data;
	vb->vb_tail = vb->vb_head + read;

	TAILQ_INSERT_TAIL(&vp->vp_rx_bufq, vb, vb_queue);

	vp->vp_rx_bufq_bytes += read;

	if (pulled)
		*pulled = read;

	return (0);
}

int
ps3vuart_port_init(struct ps3vuart_port *vp, int port)
{
	uint64_t status, bufsize;
	int err;

	vp->vp_port = port;

	PS3VUART_PORT_LOCK_INIT(vp);

	PS3VUART_PORT_LOCK(vp);

	/* Assert that VUART is connected */

	err = lv1_get_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_INTR_STAT, &status);
	if (err)
		goto out;

	if (!(status & PS3VUART_PORT_INTR_CONNECT)) {
		err = ENODEV;
		goto out;
	}

	/* Disable all interrupts */

	vp->vp_intr_mask = 0;

	err = lv1_set_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_INTR_MASK, vp->vp_intr_mask);
	if (err)
		goto out;

	/* Init Tx/Rx queues */

	TAILQ_INIT(&vp->vp_rx_bufq);
	vp->vp_rx_bufq_bytes = 0;

	TAILQ_INIT(&vp->vp_tx_bufq);
	vp->vp_tx_bufq_bytes = 0;

	/* Reset statistic counters */

	memset(&vp->vp_stat, 0, sizeof(struct ps3vuart_port_stat));

	/* Discard stale Rx bytes */

	err = p3vuart_port_discard_rx_bytes(vp, 0, NULL);
	if (err)
		goto out;

	/* Set Tx trigger level */

	/*
	 * An Tx interrupt is generated whenever the amount of
	 * occupied space in VUART Tx buffer becomes less than or
	 * equal the Tx trigger level.
	 */

	err = lv1_set_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_TX_TRIG, 1);
	if (err)
		goto out;

	/* Set Rx trigger level */

	/*
	 * An Rx interrupt is generated whenever the amount of
	 * free space in VUART Rx buffer becomes less than or
	 * equal the Rx trigger level.
	 */

	err = lv1_get_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_RX_BUFSIZE, &bufsize);
	if (err)
		goto out;

	err = lv1_set_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_RX_TRIG, bufsize - 1);
	if (err)
		goto out;

	/* Enable Rx and disconnect interrupts */

	err = ps3vuart_port_set_intr_mask(vp,
	    PS3VUART_PORT_INTR_RX | PS3VUART_PORT_INTR_DISCONNECT);

out:

	PS3VUART_PORT_UNLOCK(vp);

	if (err)
		PS3VUART_PORT_LOCK_DESTROY(vp);

	return (err);
}

void
ps3vuart_port_fini(struct ps3vuart_port *vp)
{
	struct ps3vuart_port_buf *vb;

	PS3VUART_PORT_LOCK(vp);

	/* Disable interrupts */

	ps3vuart_port_set_intr_mask(vp, 0);

	/* Free Rx queue */

	while ((vb = TAILQ_FIRST(&vp->vp_rx_bufq))) {
		TAILQ_REMOVE(&vp->vp_rx_bufq, vb, vb_queue);
		free(vb, M_PS3VUART_PORT);
	}

	/* Free Tx queue */

	while ((vb = TAILQ_FIRST(&vp->vp_tx_bufq))) {
		TAILQ_REMOVE(&vp->vp_tx_bufq, vb, vb_queue);
		free(vb, M_PS3VUART_PORT);
	}

	PS3VUART_PORT_UNLOCK(vp);

	PS3VUART_PORT_LOCK_DESTROY(vp);
}

int
ps3vuart_port_write(struct ps3vuart_port *vp, const char *buf, int size,
    int *written)
{
	struct ps3vuart_port_buf *vb;
	uint64_t bytes;
	int err = 0;

	PS3VUART_PORT_LOCK(vp);

	*written = 0;

	/* Try to send as many bytes as possible to VUART */

	if (TAILQ_EMPTY(&vp->vp_tx_bufq)) {
		err = lv1_write_virtual_uart(vp->vp_port,
		    vtophys(buf), size, &bytes);
		if (err) {
			err = ENXIO;
			goto out;
		}

		buf += bytes;
		size -= bytes;
		*written += bytes;
		vp->vp_stat.vs_tx_bytes += bytes;
	}

	if (!size)
		goto out;

	/* Enqueue remaining bytes in Tx queue */

	vb = malloc(sizeof(*vb) + size, M_PS3VUART_PORT, M_WAITOK);
	if (!vb) {
		err = ENOMEM;
		goto out;
	}

	bcopy(buf, vb->vb_data, size);

	vb->vb_head = vb->vb_data;
	vb->vb_tail = vb->vb_head + size;

	TAILQ_INSERT_TAIL(&vp->vp_tx_bufq, vb, vb_queue);

	*written += size;
	vp->vp_tx_bufq_bytes += size;

	/* Enable Tx interrupts */

	err = ps3vuart_port_set_intr_mask(vp,
	    vp->vp_intr_mask | PS3VUART_PORT_INTR_TX);

out:

	PS3VUART_PORT_UNLOCK(vp);

	return (err);
}

int
ps3vuart_port_read(struct ps3vuart_port *vp, char *buf, int size,
    int *read)
{
	struct ps3vuart_port_buf *vb, *tmp;
	int bytes;
	int err = 0;

	PS3VUART_PORT_LOCK(vp);

	err = ps3vuart_port_pull(vp, NULL);
	if (err) {
		err = ENXIO;
		goto out;
	}

	*read = 0;

	TAILQ_FOREACH_SAFE(vb, &vp->vp_rx_bufq, vb_queue, tmp) {
		bytes = min(vb->vb_tail - vb->vb_head, size);

		bcopy(vb->vb_head, buf, bytes);

		buf += bytes;
		size -= bytes;
		*read += bytes;
		vp->vp_rx_bufq_bytes -= bytes;

		if (bytes < (vb->vb_tail - vb->vb_head)) {
			vb->vb_head += bytes;
			goto out;
		}

		TAILQ_REMOVE(&vp->vp_rx_bufq, vb, vb_queue);
		free(vb, M_PS3VUART_PORT);
	}

out:

	PS3VUART_PORT_UNLOCK(vp);

	return (err);
}

int
ps3vuart_port_set_param(struct ps3vuart_port *vp, int param, uint64_t val)
{
	int err;

	PS3VUART_PORT_LOCK(vp);

	if (param == PS3VUART_PORT_PARAM_INTR_MASK)
		err = ps3vuart_port_set_intr_mask(vp, val);
	else
		err = lv1_set_virtual_uart_param(vp->vp_port, param, val);

	PS3VUART_PORT_UNLOCK(vp);

	return (err);
}

int
ps3vuart_port_get_param(struct ps3vuart_port *vp, int param, uint64_t *val)
{
	int err;

	PS3VUART_PORT_LOCK(vp);

	err = lv1_get_virtual_uart_param(vp->vp_port, param, val);
	if (err)
		goto out;

	switch (param) {
	case PS3VUART_PORT_PARAM_RX_BYTES:
		*val += vp->vp_rx_bufq_bytes;
	break;
	case PS3VUART_PORT_PARAM_INTR_STAT:
		*val &= vp->vp_intr_mask;
	break;
	}

out:

	PS3VUART_PORT_UNLOCK(vp);

	return (err);
}

void
ps3vuart_port_intr(struct ps3vuart_port *vp)
{
	uint64_t status;
	int err;

	PS3VUART_PORT_LOCK(vp);

	err = lv1_get_virtual_uart_param(vp->vp_port,
	    PS3VUART_PORT_PARAM_INTR_STAT, &status);
	if (err)
		goto out;

	KASSERT(status & PS3VUART_PORT_INTR_CONNECT,
	    ("port %d disconnected", vp->vp_port));
	KASSERT(!(status & PS3VUART_PORT_INTR_DISCONNECT),
	    ("port %d disconnected", vp->vp_port));

	status &= vp->vp_intr_mask;

	if (status & PS3VUART_PORT_INTR_RX) {
		vp->vp_stat.vs_rx_intrs++;
		ps3vuart_port_pull(vp, NULL);
	}

	if (status & PS3VUART_PORT_INTR_TX) {
		vp->vp_stat.vs_tx_intrs++;
		ps3vuart_port_push(vp, NULL);
	}

out:

	PS3VUART_PORT_UNLOCK(vp);
}

void
ps3vuart_port_get_stat(struct ps3vuart_port *vp,
    struct ps3vuart_port_stat *stat)
{
	PS3VUART_PORT_LOCK(vp);

	*stat = vp->vp_stat;

	PS3VUART_PORT_UNLOCK(vp);
}

static int
ps3vuart_port_modevent(module_t mod, int type, void *data)
{
	int err = 0;

	switch (type) {
	case MOD_LOAD:
	case MOD_UNLOAD:
	break;
	default:
		err = EOPNOTSUPP;
	}

	return (err);
}

static moduledata_t ps3vuart_port_mod = {
	"ps3vuart_port",
	ps3vuart_port_modevent,
	NULL
};

DECLARE_MODULE(ps3vuart_port, ps3vuart_port_mod, SI_SUB_PSEUDO, SI_ORDER_ANY);
MODULE_VERSION(ps3vuart_port, 1);
