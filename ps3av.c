/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/conf.h>
#include <sys/bus.h>
#include <sys/malloc.h>
#include <sys/lock.h>
#include <sys/mutex.h>
#include <sys/uio.h>
#include <sys/resource.h>
#include <sys/rman.h>

#include <vm/vm.h>
#include <vm/pmap.h>

#include <machine/bus.h>
#include <machine/platform.h>
#include <machine/pmap.h>
#include <machine/resource.h>

#include "ps3vuart_bus.h"
#include "ps3vuart_port.h"
#include "ps3av_msg.h"
#include "ps3av.h"

#define PS3AV_LOCK_INIT(_sc) \
    mtx_init(&(_sc)->sc_mtx, device_get_nameunit((_sc)->sc_dev), \
        "ps3av", MTX_DEF)
#define PS3AV_LOCK_DESTROY(_sc)		mtx_destroy(&(_sc)->sc_mtx)
#define PS3AV_LOCK(_sc)			mtx_lock(&(_sc)->sc_mtx)
#define	PS3AV_UNLOCK(_sc)		mtx_unlock(&(_sc)->sc_mtx)
#define PS3AV_ASSERT_LOCKED(_sc)	mtx_assert(&(_sc)->sc_mtx, MA_OWNED)
#define PS3AV_ASSERT_UNLOCKED(_sc)	mtx_assert(&(_sc)->sc_mtx, MA_NOTOWNED)

struct ps3av_softc {
	device_t sc_dev;

	struct mtx sc_mtx;

	int sc_irq_id;
	struct resource	*sc_irq;
	void *sc_irq_ctx;

	struct ps3vuart_port sc_port;

	struct cdev *sc_cdev;
};

static void ps3av_intr(void *arg);
static int ps3av_init(struct ps3av_softc *sc);
static int ps3av_fini(struct ps3av_softc *sc);

static int ps3av_dev_open(struct cdev *dev, int flags, int mode,
    struct thread *td);
static int ps3av_dev_close(struct cdev *dev, int flags, int mode,
    struct thread *td);
static int ps3av_dev_read(struct cdev *dev, struct uio *uio, int ioflag);
static int ps3av_dev_write(struct cdev *dev, struct uio *uio, int ioflag);
static int ps3av_dev_poll(struct cdev *dev, int events, struct thread *td);

static MALLOC_DEFINE(M_PS3AV, "ps3av", "PS3 AV");

static struct cdevsw ps3av_cdevsw = {
	.d_version	= D_VERSION,
	.d_open		= ps3av_dev_open,
	.d_close	= ps3av_dev_close,
	.d_read		= ps3av_dev_read,
	.d_write	= ps3av_dev_write,
	.d_poll		= ps3av_dev_poll,
	.d_name		= "ps3av",
};

static device_t ps3av_dev = NULL;

static int 
ps3av_probe(device_t dev) 
{
	if (ps3vuart_bus_get_type(dev) != PS3VUART_BUS_TYPE_AV)
		return (ENXIO);

	device_set_desc(dev, "Playstation 3 AV");

	return (BUS_PROBE_SPECIFIC);
}

static int
ps3av_attach(device_t dev)
{
	struct ps3av_softc *sc = device_get_softc(dev);
	int err;

	sc->sc_dev = dev;

	PS3AV_LOCK_INIT(sc);

	PS3AV_LOCK(sc);

	/* Setup IRQ */

	sc->sc_irq_id = 0;
	sc->sc_irq = bus_alloc_resource_any(dev, SYS_RES_IRQ,
	    &sc->sc_irq_id, RF_ACTIVE);
	if (!sc->sc_irq) {
		device_printf(dev, "Could not allocate IRQ\n");
		err = ENXIO;
		goto destroy_lock;
	}

	err = bus_setup_intr(dev, sc->sc_irq,
	    INTR_TYPE_MISC | INTR_MPSAFE,
	    NULL, ps3av_intr, sc, &sc->sc_irq_ctx);
	if (err) {
		device_printf(dev, "Could not setup IRQ (%d)\n", err);
		goto release_irq;
	}

	/* Setup VUART port */

	err = ps3vuart_port_init(&sc->sc_port, ps3vuart_bus_get_port(dev));
	if (err) {
		device_printf(dev, "Could not setup VUART port (%d)\n", err);
		goto teardown_irq;
	}

	/* Setup AV */

	err = ps3av_init(sc);
	if (err) {
		device_printf(dev, "Could not setup AV (%d)\n", err);
		goto fini_vuart_port;
	}

	/* Create char device */

	sc->sc_cdev = make_dev(&ps3av_cdevsw, 0, UID_ROOT, GID_WHEEL, 0600,
	    "%s", "ps3av");
	if (!sc->sc_cdev) {
		device_printf(dev, "Could not create char device\n");
		err = ENOMEM;
		goto fini_av;
	}

	sc->sc_cdev->si_drv1 = sc;

	ps3av_dev = dev;

	PS3AV_UNLOCK(sc);

	return (0);

fini_av:

	ps3av_fini(sc);

fini_vuart_port:

	ps3vuart_port_fini(&sc->sc_port);

teardown_irq:

	bus_teardown_intr(dev, sc->sc_irq, sc->sc_irq_ctx);

release_irq:

	bus_release_resource(dev, SYS_RES_IRQ, sc->sc_irq_id, sc->sc_irq);

destroy_lock:

	PS3AV_UNLOCK(sc);

	PS3AV_LOCK_DESTROY(sc);

	return (err);
}

static int
ps3av_detach(device_t dev)
{
	struct ps3av_softc *sc = device_get_softc(dev);

	PS3AV_LOCK(sc);

	ps3av_dev = NULL;

	/* Destroy char device */

	destroy_dev(sc->sc_cdev);

	/* Shutdown AV */

	ps3av_fini(sc);

	/* Free IRQ */

	bus_teardown_intr(dev, sc->sc_irq, sc->sc_irq_ctx);
	bus_release_resource(dev, SYS_RES_IRQ, sc->sc_irq_id, sc->sc_irq);

	/* Destroy VUART port */

	ps3vuart_port_fini(&sc->sc_port);

	PS3AV_UNLOCK(sc);

	PS3AV_LOCK_DESTROY(sc);

	return (0);
}

static void
ps3av_intr(void *arg)
{
	struct ps3av_softc *sc = arg;

	PS3AV_LOCK(sc);

	ps3vuart_port_intr(&sc->sc_port);

	PS3AV_UNLOCK(sc);
}

static int
ps3av_init(struct ps3av_softc *sc)
{
	PS3AV_ASSERT_LOCKED(sc);

	return (0);
}

static int
ps3av_fini(struct ps3av_softc *sc)
{
	PS3AV_ASSERT_LOCKED(sc);

	return (0);
}

int
ps3av_set_video_mode(int mode)
{
	struct ps3av_softc *sc;

	if (!ps3av_dev)
		return (ENODEV);

	sc = device_get_softc(ps3av_dev);

	PS3AV_LOCK(sc);

	PS3AV_UNLOCK(sc);

	return (ENODEV);
}

static int
ps3av_dev_open(struct cdev *dev, int flags, int mode,
    struct thread *td)
{
	struct ps3av_softc *sc = dev->si_drv1;

	PS3AV_LOCK(sc);

	PS3AV_UNLOCK(sc);

	return (0);
}

static int
ps3av_dev_close(struct cdev *dev, int flags, int mode,
    struct thread *td)
{
	struct ps3av_softc *sc = dev->si_drv1;

	PS3AV_LOCK(sc);

	PS3AV_UNLOCK(sc);

	return (0);
}

static int
ps3av_dev_read(struct cdev *dev, struct uio *uio, int ioflag)
{
#define BUFSZ	4096

	struct ps3av_softc *sc = dev->si_drv1;
	unsigned char *buf;
	int read;
	int err = 0;

	PS3AV_LOCK(sc);

	if (!uio->uio_resid)
		goto out;

	buf = malloc(BUFSZ, M_PS3AV, M_WAITOK);
	if (!buf) {
		err = ENOMEM;
		goto out;
	}

	while (uio->uio_resid) {
		err = ps3vuart_port_read(&sc->sc_port, buf,
		    min(uio->uio_resid, BUFSZ), &read);
		if (err || !read)
			break;

		err = uiomove(buf, read, uio);
		if (!err)
			break;
	}

	free(buf, M_PS3AV);

out:

	PS3AV_UNLOCK(sc);

	return (err);

#undef BUFSZ
}

static int
ps3av_dev_write(struct cdev *dev, struct uio *uio, int ioflag)
{
#define BUFSZ	4096

	struct ps3av_softc *sc = dev->si_drv1;
	unsigned char *buf;
	int bytes, written;
	int err = 0;

	PS3AV_LOCK(sc);

	if (!uio->uio_resid)
		goto out;

	buf = malloc(BUFSZ, M_PS3AV, M_WAITOK);
	if (!buf) {
		err = ENOMEM;
		goto out;
	}

	while (uio->uio_resid) {
		bytes = min(uio->uio_resid, BUFSZ);

		err = uiomove(buf, bytes, uio);
		if (err)
			break;

		err = ps3vuart_port_write(&sc->sc_port, buf, bytes, &written);
		if (err || !written)
			break;
	}

	free(buf, M_PS3AV);

out:

	PS3AV_UNLOCK(sc);

	return (err);

#undef BUFSZ
}

static int
ps3av_dev_poll(struct cdev *dev, int events, struct thread *td)
{
	struct ps3av_softc *sc = dev->si_drv1;

	PS3AV_LOCK(sc);

	/* XXX: implement */

	PS3AV_UNLOCK(sc);

	return (0);
}

static device_method_t ps3av_methods[] = {
	/* Device interface */
	DEVMETHOD(device_probe,		ps3av_probe),
	DEVMETHOD(device_attach,	ps3av_attach),
	DEVMETHOD(device_detach,	ps3av_detach),

	{ 0, 0 }
};

static driver_t ps3av_driver = {
	"ps3av",
	ps3av_methods,
	sizeof(struct ps3av_softc)
};

static devclass_t ps3av_devclass;

DRIVER_MODULE(ps3av, ps3vuart_bus, ps3av_driver, ps3av_devclass, 0, 0);
MODULE_DEPEND(ps3av, ps3vuart_port, 1, 1, 1);
MODULE_VERSION(ps3av, 1);
