/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3DM_MSG_H
#define _PS3DM_MSG_H

#define PS3DM_PID(fid, id)	((fid) | (id))
#define PS3DM_FID(pid)		((pid) & ~0xffful)

enum ps3dm_function_packet {
	/* Virtual TRM */

	PS3DM_FID_VTRM			= 0x00002000,
	PS3DM_PID_VTRM_INIT		= PS3DM_PID(PS3DM_FID_VTRM, 1),
	PS3DM_PID_VTRM_GET_STATUS	= PS3DM_PID(PS3DM_FID_VTRM, 2),

	/* Secure RTC */

	PS3DM_FID_SRTC			= 0x00003000,
	PS3DM_PID_SRTC_GET_TIME		= PS3DM_PID(PS3DM_FID_SRTC, 2),
	PS3DM_PID_SRTC_SET_TIME		= PS3DM_PID(PS3DM_FID_SRTC, 3),

	/* Storage manager */

	PS3DM_FID_SM			= 0x00005000,
	PS3DM_PID_SM_GET_RND		= PS3DM_PID(PS3DM_FID_SM, 3),
};

struct ps3dm_header {
	uint32_t tag;
	uint32_t fid;			/* enum ps3dm_function_packet */
	uint32_t payload_length;
	uint32_t reply_length;
};

#define PS3DM_HDR(_p)	((struct ps3dm_header *) (_p))

struct ps3dm_ss_header {
	uint64_t pid;			/* enum ps3dm_function_packet */
	uint64_t fid;			/* enum ps3dm_function_packet */
	uint32_t status;
	uint8_t res[4];
	uint64_t laid;
	uint64_t paid;
};

#define PS3DM_SS_HDR(_p)	((struct ps3dm_ss_header *) (_p))

struct ps3dm_sm_get_rnd {
	struct ps3dm_header dm_hdr;
	struct ps3dm_ss_header ss_hdr;
	uint8_t rnd[24];
};

static inline void
ps3dm_init_header(struct ps3dm_header *hdr, uint32_t tag, uint32_t fid,
    uint32_t payload_length, uint32_t reply_length)
{
	hdr->tag = tag;
	hdr->fid = fid;
	hdr->payload_length = payload_length;
	hdr->reply_length = reply_length;
}

static inline void
ps3dm_init_ss_header( struct ps3dm_ss_header *hdr, uint64_t pid,
    uint64_t laid, uint64_t paid)
{
	hdr->pid = pid;
	hdr->fid = PS3DM_FID(pid);
	hdr->laid = laid;
	hdr->paid = paid;
}

#endif /* _PS3DM_MSG_H */
